#!/usr/bin/env python

import os, sys, subprocess
from optparse import OptionParser

domain = None
aliases = None
no_ssl = None


# certbot renew --pre-hook "service httpd stop" --post-hook "service httpd start"

def main():
    global domain, aliases, no_ssl

    parser = OptionParser(usage="usage: %prog [options] filename",
                          version="%prog 1.0")
    parser.add_option("-d", "--domain", dest="domain", help="create a virtualhost for DOMAIN")
    parser.add_option("-a", "--aliases", dest="aliases", help="additional FQDN aliases for DOMAIN")
    parser.add_option("--no-ssl", dest="no_ssl", action="store_true",
                      help="don't create SSL certs or configure SSL vhost")
    (options, args) = parser.parse_args()

    # Check a domain has been provided
    if not options.domain:
        parser.print_help()
        parser.error("Domain not given")

    # Set options
    domain = options.domain
    aliases = options.aliases
    no_ssl = options.no_ssl

    # Check that the vhost does not exist
    if check_vhost_exists():
        sys.exit("Virtualhost for " + options.domain + " already exists")

    # Create the virtualhost
    create_vhost()


def create_vhost():
    global no_ssl

    # Create vhost file
    create_vhost_file()

    # Create the web directory
    create_vhost_dir()

    # Enable the vhost
    enable_vhost()

    # Reload apache
    reload_apache()

    # Generate SSL certificate
    if not no_ssl:
        generate_certs()


def create_vhost_file():
    global domain, aliases, no_ssl

    # Choose the configuration file
    if no_ssl:
        filename = '../conf/http.conf'
    else:
        filename = '../conf/https.conf'

    f = open(filename, 'r')
    vhost = f.read()
    f.close()

    # Replace information within the configuration file
    vhost = vhost.replace('domain.tld', domain)

    if not aliases:
        vhost = vhost.replace('[AdditionalAliases]', '')
    else:
        vhost = vhost.replace('[AdditionalAliases]', aliases)

    # Write data to the vhost configuration file
    vhostFile = open('/etc/httpd/conf/sites-available/{}.conf'.format(domain), 'w+')
    vhostFile.write(vhost)
    vhostFile.close()


def create_vhost_dir():
    directory = '/var/www/{}'.format(domain)

    if not os.path.exists(directory):
        os.makedirs(directory)


def enable_vhost():
    global domain

    os.symlink('/etc/httpd/conf/sites-available/{}.conf'.format(domain),
               '/etc/httpd/conf/sites-enabled/{}.conf'.format(domain))


def reload_apache():
    subprocess.call('service httpd reload', shell=True)


def generate_certs():
    subprocess.call('certbot --apache certonly', shell=True)


def check_vhost_exists():
    global domain

    if os.path.isfile('/etc/httpd/conf/sites-available/{}.conf'.format(domain)):
        return True
    else:
        return False


if __name__ == '__main__':
    main()
