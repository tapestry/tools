#!/bin/sh

echo Enabling $1...

ln -s /etc/httpd/conf/sites-available/$1.conf /etc/httpd/conf/sites-enabled/$1.conf
