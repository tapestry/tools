#!/bin/sh

######################################
##                                  ##
## Automatically add Bitbucket repo ##
##                                  ##
######################################

# Syntax:
#
# 	./createrepo.sh [-w site-name your-email] repo-proper-name repo-slug path-to-repo
#
# The script will output a newly generated RSA SSH public key which is to be used as the Bitbucket deployment key.
#

## Check amount of arguments
if [ "$#" -lt 3 ]
then
	echo 
	echo "Syntax:"
	echo "./createrepo.sh [-w site-name your-email] repo-proper-name repo-slug path-to-repo"
	echo
	echo -e "-w SITE-NAME YOUR-EMAIL\t\tWill create a full WordPress installation under SITE-NAME using the latest WP. You will be prompted for WP installation options."
	echo
	echo "Example:"
	echo "./createrepo.sh "Test Repository" test-repository /var/www/test/wp-content/themes/test-theme"
	echo
	exit
fi

echo "##########################################"
echo "##                                      ##"
echo "## Automatic Bitbucket repository clone ##"
echo "##                                      ##"
echo "##########################################"
echo 

##  Check which options have been applied and set variables accordingly
if [ $1 == "-w" ]
then
	
	echo "Create full WordPress installation"
	
	sitename=$2
	email=$3
	fullname=$4
	slug=$5
	repopath=$6
	reponame=$(basename "$6")
	basedir=$(dirname "$6")
	
else

	fullname=$1
	slug=$2
	repopath=$3
	reponame=$(basename "$3")
	basedir=$(dirname "$3")

fi

#######################################################
## Sort out files/directories and set some variables ##
#######################################################

hostname=`hostname`

# chown -R apache:apache /var/www/
chown -R apache:apache /usr/share/httpd/

## Create .ssh if it doesn't exist
if [ ! -d "/usr/share/httpd/.ssh" ]
then
	sudo -u apache mkdir -p /usr/share/httpd/.ssh
fi

## Create .ssh/config if it doesn't exist
if [ ! -f "/usr/share/httpd/.ssh/config" ]
then
	sudo -u apache touch /usr/share/httpd/.ssh/config
fi

#############
# Bitbucket #
#############

## Create path directory if it does not already exist
if [ ! -d "$basedir" ]
then
	sudo -u apache mkdir -p "$basedir"
fi

## Generate deployment key

echo "Generating a deployment key for use on Bitbucket"
echo "*** Please use a blank passphrase for the public/private rsa key pair ***"
echo 

sudo -u apache ssh-keygen -t rsa -f "/usr/share/httpd/.ssh/$slug"

depkey=$(cat "/usr/share/httpd/.ssh/$slug.pub")

## Append Bitbucket deployment private key block to SSH config file

sudo -u apache echo -e "Host $slug" >> "/usr/share/httpd/.ssh/config"
sudo -u apache echo -e "\tHostName bitbucket.org" >> "/usr/share/httpd/.ssh/config"
sudo -u apache echo -e "\tIdentityFile ~/.ssh/$slug" >> "/usr/share/httpd/.ssh/config"
sudo -u apache echo -e "\tUser Git" >> "/usr/share/httpd/.ssh/config"
sudo -u apache echo "" >> "/usr/share/httpd/.ssh/config"

echo 
echo "Deployment key:"
echo 
echo $depkey
echo
echo "Please open the repository on bitbucket.org, and add this key as a deployment key in the options. Once the key has been added, cloning may continue." 
echo

while true; do
	read -p "Have you added the deployment key? [y] " y
	case $y in
		[Yy]* ) break;;
		* ) echo "Please add the deployment key";;
	esac
done

echo

## Start to clone to repository

sudo -u apache git clone "git@$slug:tapestry/$slug.git" "$repopath"

echo 
