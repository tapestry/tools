# Server scripts and tools repository
__It is recommended to clone this repository into__ `/root`

## conf
This folder contains a dummy HTTP and HTTPS server configuration files. The HTTP file comes pre-configured with secure
SSL ciphers.

## git-deploy
These are the git-deploy files required to receive POST's from Bitbucket to automatically update Git repositories

## httpd-scipts
The `enablesite.sh` and `disablesite.sh` scripts are for symlinking and enabling virtual host configuration files in the
Digital servers.
 
## serversetup.sh / configserver.sh
__This was pre-data centre and for use with Rackspace. These files are no longer used.__

Run `serversetup.sh` from your local machine to automatically configure a CentOS machine to become a web server with 
programs and Tapestry public keys already added.

## createrepo.sh / createrepo7.sh
__These scripts should always be used to clone a repository__

These scripts pull down a Tapestry Bitbucket repository onto a CentOS machine. It covers the `.ssh` hostname config
automatically from the repository slug. There are different scripts for CentOS 6.5 and 7 as the home directories of the
`apache` user differs between versions.

### Installation
CentOS 6.5:

    ln -s ~/tools/createrepo.sh /usr/bin/createrepo
    
CentOS 7:

    ln -s ~/tools/createrepo7.sh /usr/bin/createrepo
    
### Usage
    createrepo a repo-slug /path/to/repo
    
Where `repo-slug` is the Bitbucket slug (this can be seen in the URL), and `/path/to/repo` points to the folder you want
to clone into

`a` can technically be replaced with anything as the parameter is not used.

## dssh.sh
This script was created after we introduced two new digital servers and spread the websites out. It's syntax is exactly 
the same as normal SSH, but it will only work for Tapestry Digital servers. It first performs an `nslookup` to determine
the external IP of the server, and connect to the server.

### Usage
As an example, creativedebuts.co.uk is hosted on digital3.tapestry.co.uk. Instead of having to remember the server, you 
can type the following: `dssh root@creativedebuts.co.uk`. This will produce the following output:

    tapestry177:~ james.austen$ dssh root@creativedebuts.co.uk
    Connecting to digital3.tapestry.co.uk [83.244.146.107]
    Last login: Thu Jan  7 13:53:11 2016 from 10.2.5.63
    [root@digital3 ~]#

