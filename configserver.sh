#!/bin/sh

password=$1
websiteurl=$2
phpver=$3
output=$4

if [ "$#" -lt 4 ]
then
	echo "Too few arguments: "
	echo $password
	echo $websiteurl
	echo $phpver
	exit
fi

echo "Server configuration script downloaded and excuted on remote server..."
echo ""

## Download all of the Tapestry Digital public keys
wget http://www.tapestry.co.uk/keys.txt -O - >> /root/.ssh/authorized_keys

sleep 3

echo "Continuing installation..."
echo ""

sleep 2

echo "Installing web server utilities for PHP $phpver..."
echo ""

if [ $phpver == 5.4 ]
then
	
	{
		rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm
	
		yum -y install mysql-server mysql httpd php54w php54w-mysqlnd php54w-xml php54w-gd unzip nano git memcached
	} &> "$output"

else
	
	{
		yum -y install mysql-server mysql httpd php php-mysql unzip php-xml php-gd php-pecl-apc php-pecl-memcache nano git memcached
	} &> "$output"
	
fi

## Non-PHP specific installation

{
	chkconfig httpd on
	chkconfig mysqld on
} &> "$output"

echo "Creating '$websiteurl' at /var/www/$websiteurl"

mkdir -p "/var/www/$websiteurl"

virtualblock="\n<VirtualHost *:80>\n\tDocumentRoot /var/www/$websiteurl\n\tServerName  $websiteurl\n\tErrorLog logs/$websiteurl-error_log\n\tCustomLog logs/$websiteurl-access_log common\n\t<Directory /var/www/$websiteurl>\n\t\tallow from all\n\t\tOptions +Indexes\n\t\tAllowOverride All\n\t</Directory>\n</VirtualHost>"

echo -e "$virtualblock" >> "/etc/httpd/conf/httpd.conf"

{
	service httpd start
	
	iptables -I INPUT -p tcp --dport 80 -j ACCEPT
	
	service iptables save
	
	service mysqld start
	
	mysqladmin -u root password "$password"
	
	chown -R apache:apache /var/www
} &> "$output"

echo ""
echo "Web server installation completed."
echo ""
echo "Installing yum auto update, Rackspace monitoring tools and backup agent..."
echo ""

sleep 3

## Yum auto update

{
	yum -y install yum-cron
	chkconfig yum-cron on
	service yum-cron start
	
	## Rackspace Monitoring tools
	
	curl https://monitoring.api.rackspacecloud.com/pki/agent/linux.asc > /tmp/linux.asc
	
	sudo rpm --import /tmp/linux.asc
	
	touch /etc/yum.repos.d/rackspace-cloud-monitoring.repo
	
	echo -e "[rackspace]\nname=Rackspace Monitoring\nbaseurl=http://stable.packages.cloudmonitoring.rackspace.com/centos-6-x86_64\nenabled=1" >> /etc/yum.repos.d/rackspace-cloud-monitoring.repo
	
	yum -y install rackspace-monitoring-agent
	
	rackspace-monitoring-agent --setup
	
	service rackspace-monitoring-agent start
	
	chkconfig rackspace-monitoring-agent on
	
	## Install Rackspace Backup Agent
	
	wget -O /etc/yum.repos.d/drivesrvr.repo "http://agentrepo.drivesrvr.com/redhat/drivesrvr.repo"
	
	yum -y install driveclient
	
	/usr/local/bin/driveclient –configure
	
	service driveclient start
	
	chkconfig driveclient on
} &> "$output"

echo "Installion completed"
echo ""

rm -f /root/configserver.sh