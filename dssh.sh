#!/bin/sh

#############
## DSSH
##
## Connects to the correct production server for a specified host
##
## ln -s ~/tools/dssh.sh /usr/bin/dssh
##
## Author: James Austen <james.austen@tapestry.co.uk>
#############

# Split our user@host input string
USER=`echo $1 | awk -F'@' '{print $1}'`
HOST=`echo $1 | awk -F'@' '{print $2}'`

# Perform NSLOOKUP on the host and fetch the IP
IP=`nslookup ${HOST} | awk '/Address: / {print $2}'`

## Check if the IP is a valid Tapestry Production Server
if [ "$IP" = "5.148.10.211" ];
then
    SERVER=digital1.tapestry.co.uk
elif [ "$IP" = "83.244.146.107" ];
then
    SERVER=digital3.tapestry.co.uk
elif [ "$IP" = "83.244.146.108" ];
then
    SERVER=digital4.tapestry.co.uk
else
    SERVER=NONE
fi

## Run the SSH commands
if [ "$SERVER" != "NONE" ];
then
    echo "Connecting to ${HOST} on ${SERVER} [${IP}]"
    ssh "${USER}@${SERVER}" $2
else
    echo "$IP is not a Tapestry production server"
fi